package exness.iph.exnesstestapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import exness.iph.exnesstestapplication.connectivity.ClientWebSocket;
import exness.iph.exnesstestapplication.fragment.CurrencyFragment;
import exness.iph.exnesstestapplication.presenter.CurrencyPresenter;
import exness.iph.exnesstestapplication.repository.RemoteTicksRepository;
import exness.iph.exnesstestapplication.storage.PreferenceManager;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        showFirstScreen();
    }

    private void showFirstScreen() {
        CurrencyFragment currencyFragment = new CurrencyFragment();
        getFragmentManager().beginTransaction().replace(R.id.content, currencyFragment)
                .commit();
    }

}
