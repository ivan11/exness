package exness.iph.exnesstestapplication.connectivity;

/**
 * Created by ivanphytsyk on 5/29/17.
 */

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.SSLContext;

import exness.iph.exnesstestapplication.BuildConfig;
import exness.iph.exnesstestapplication.repository.RemoteTicksRepository;
import exness.iph.exnesstestapplication.repository.TickCallback;
import exness.iph.exnesstestapplication.repository.model.CurrencyPair;
import exness.iph.exnesstestapplication.repository.model.Tick;


public class ClientWebSocket implements RemoteTicksRepository {

    private String host = BuildConfig.HOST;
    private WebSocket webSocket;
    private TickCallback callback;

    private Gson gson = new GsonBuilder().create();
    private Set<CurrencyPair> subscribeOn;

    private void connect() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (webSocket != null) {
                    reconnect();
                } else {
                    try {
                        WebSocketFactory factory = new WebSocketFactory();
                        SSLContext context = NaiveSSLContext.getInstance("TLS");
                        factory.setSSLContext(context);
                        webSocket = factory.createSocket(host);
                        webSocket.addListener(new SocketListener());
                        webSocket.connect();
                    } catch (WebSocketException | IOException | NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                }
            }

        }).start();
    }

    private void reconnect() {
        try {
            webSocket = webSocket.recreate().connect();
        } catch (WebSocketException | IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        if (webSocket != null) {
            webSocket.disconnect();
        }
    }

    private class SocketListener extends WebSocketAdapter {

        @Override
        public void onConnected(WebSocket websocket, Map<String, List<String>> headers) throws Exception {
            super.onConnected(websocket, headers);
            webSocket.sendText(String.format(Locale.getDefault(), "SUBSCRIBE: %s", TextUtils.join(",", subscribeOn)));
        }

        public void onTextMessage(WebSocket websocket, String message) {
            List<Tick> parsedTicks = parseTicks(message);
            callback.onTickReceived(parsedTicks);
        }

        private List<Tick> parseTicks(String message) {
            if (message.contains("subscribed_count")) {
                TickResponse tickResponse = gson.fromJson(message, TickResponse.class);
                if (tickResponse.getSubscribedList() != null && tickResponse.getSubscribedList().getTicks() != null) {
                    return new ArrayList<>(new HashSet<>(tickResponse.getSubscribedList().getTicks()));
                }
            } else {
                SubscribedList tickResponse = gson.fromJson(message, SubscribedList.class);
                if (tickResponse.getTicks() != null) {
                    return new ArrayList<>(new HashSet<>(tickResponse.getTicks()));
                }
            }
            return null;
        }

        @Override
        public void onError(WebSocket websocket, WebSocketException cause) {
            reconnect();
        }

        @Override
        public void onDisconnected(WebSocket websocket,
                                   WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame,
                                   boolean closedByServer) {
            if (closedByServer) {
                reconnect();
            }
        }

        @Override
        public void onUnexpectedError(WebSocket websocket, WebSocketException cause) {
            reconnect();
        }

        @Override
        public void onPongFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
            super.onPongFrame(websocket, frame);
            websocket.sendPing("Are you there?");
        }
    }

    @Override
    public void subscribe(Set<CurrencyPair> subscribeOn, TickCallback callback) {
        this.callback = callback;
        this.subscribeOn = subscribeOn;
        close();
        connect();
    }

    @Override
    public void unsubscribe() {
        webSocket.sendText(String.format(Locale.getDefault(), "UNSUBSCRIBE: %s", TextUtils.join(",", subscribeOn)));
        close();
    }

    @Override
    public boolean isSubscribed() {
        return webSocket != null && webSocket.isOpen();
    }
}
