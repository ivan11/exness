package exness.iph.exnesstestapplication.repository;

import java.util.List;
import java.util.Set;

import exness.iph.exnesstestapplication.repository.model.CurrencyPair;
import exness.iph.exnesstestapplication.repository.model.Tick;

/**
 * Created by ivanphytsyk on 5/29/17.
 */

public interface LocalTicksRepository {
    Set<CurrencyPair> getLastSavedSubscriptions();
    void saveState(Set<CurrencyPair> currencyPairs);
    double getLastSavedCurrency(CurrencyPair currencyPair);
    void saveCurrency(CurrencyPair currencyPair, double value);
    void saveTicks(List<Tick> ticks);
    List<Tick> getTicks();
}
