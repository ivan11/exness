package exness.iph.exnesstestapplication.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import exness.iph.exnesstestapplication.R;
import exness.iph.exnesstestapplication.connectivity.ClientWebSocket;
import exness.iph.exnesstestapplication.presenter.CurrencyPresenter;
import exness.iph.exnesstestapplication.repository.model.CurrencyPair;
import exness.iph.exnesstestapplication.repository.model.Tick;
import exness.iph.exnesstestapplication.storage.PreferenceManager;
import exness.iph.exnesstestapplication.ui.SimpleItemTouchHelperCallback;
import exness.iph.exnesstestapplication.ui.adapter.CurrencyAdapter;
import exness.iph.exnesstestapplication.view.CurrencyView;

/**
 * Created by ivanphytsyk on 5/29/17.
 */

public class CurrencyFragment extends Fragment implements CurrencyView {

    private CurrencyAdapter adapter;
    private RecyclerView ticksView;

    private CurrencyPresenter currencyPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        currencyPresenter = new CurrencyPresenter(this, new PreferenceManager(getActivity()), new ClientWebSocket());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_currency, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ticksView = (RecyclerView) view.findViewById(R.id.rv_ticks);
        ticksView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        adapter = new CurrencyAdapter(getActivity(), new CurrencyAdapter.ItemCallback() {
            @Override
            public void onItemClicked(CurrencyPair currencyPair) {
                currencyPresenter.itemClicked(currencyPair);
            }

            @Override
            public void onItemDeleted(CurrencyPair currencyPair) {
                currencyPresenter.itemDeleted(currencyPair);
            }
        });
        ItemTouchHelper.Callback callback =
                new SimpleItemTouchHelperCallback(adapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(ticksView);
        ticksView.setAdapter(adapter);
    }

    @Override
    public void showTicks(final List<Tick> ticks) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (ticks != null) {
                    adapter.updateTicks(ticks);
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        currencyPresenter.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        currencyPresenter.stop();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.settings) {
            currencyPresenter.subscriptionsButtonClicked();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showSubscriptions() {
        getFragmentManager().beginTransaction().replace(R.id.content, new SubscriptionsFragment())
                .addToBackStack("").commit();
    }

    @Override
    public List<Tick> getOrderedTicks() {
        return adapter.getTicks();
    }

    @Override
    public void showChart(CurrencyPair currencyPair) {
        Fragment fragment = new ChartFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ChartFragment.EXTRA_CURRENCY_PAIR, currencyPair);
        fragment.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.content, fragment)
                .addToBackStack("chart").commit();
    }
}
