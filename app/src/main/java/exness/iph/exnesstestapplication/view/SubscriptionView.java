package exness.iph.exnesstestapplication.view;

import java.util.Set;

import exness.iph.exnesstestapplication.repository.model.CurrencyPair;

/**
 * Created by ivanphytsyk on 5/30/17.
 */

public interface SubscriptionView {
    void moveBack();

    void showSubscriptions(Set<CurrencyPair> savedState);
}
