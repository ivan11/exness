package exness.iph.exnesstestapplication.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import exness.iph.exnesstestapplication.repository.model.CurrencyPair;
import exness.iph.exnesstestapplication.repository.model.Tick;
import exness.iph.exnesstestapplication.ui.CurrencyRow;
import exness.iph.exnesstestapplication.ui.ItemTouchHelperAdapter;

/**
 * Created by ivanphytsyk on 5/29/17.
 */

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.RowHolder> implements ItemTouchHelperAdapter {

    private List<Tick> ticks = new ArrayList<>();

    private Context context;

    private ItemCallback itemCallback;

    public CurrencyAdapter(Context context, ItemCallback itemCallback) {
        this.context = context;
        this.itemCallback = itemCallback;
    }

    @Override
    public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CurrencyRow currencyRow = new CurrencyRow(context);
        return new RowHolder(currencyRow);
    }

    @Override
    public void onBindViewHolder(RowHolder holder, int position) {
        final Tick tick = ticks.get(position);
        holder.currencyRow.bindTick(tick, position % 2 == 0);
        holder.currencyRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemCallback.onItemClicked(tick.getCurrencyPair());
            }
        });
    }

    @Override
    public int getItemCount() {
        return ticks.size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(ticks, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(ticks, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemDismiss(int position) {
        Tick removedTick = ticks.remove(position);
        itemCallback.onItemDeleted(removedTick.getCurrencyPair());
        notifyItemRemoved(position);
    }

    /**
     * Added new item to list if not exist yet, update existing
     * @param updatedTicks ticks from socket
     */
    public void updateTicks(List<Tick> updatedTicks) {
        for (Tick tick: updatedTicks) {
            int index = ticks.indexOf(tick);
            if (index != -1) {
                ticks.set(index, tick);
                notifyItemChanged(index);
            } else {
                ticks.add(tick);
                notifyItemInserted(ticks.size() - 1);
            }
        }
    }

    class RowHolder extends RecyclerView.ViewHolder {
        private CurrencyRow currencyRow;

        RowHolder(CurrencyRow itemView) {
            super(itemView);
            this.currencyRow = itemView;
        }
    }

    public List<Tick> getTicks() {
        return ticks;
    }

    public interface ItemCallback {
        void onItemClicked(CurrencyPair currencyPair);
        void onItemDeleted(CurrencyPair currencyPair);
    }
}
