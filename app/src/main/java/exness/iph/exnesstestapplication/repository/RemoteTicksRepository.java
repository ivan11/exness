package exness.iph.exnesstestapplication.repository;

import java.util.List;
import java.util.Set;

import exness.iph.exnesstestapplication.repository.model.CurrencyPair;
import exness.iph.exnesstestapplication.repository.model.Tick;

/**
 * Created by ivanphytsyk on 5/29/17.
 */

public interface RemoteTicksRepository {
    void subscribe(Set<CurrencyPair> subscribeOn, TickCallback callback);
    void unsubscribe();
    boolean isSubscribed();
}
