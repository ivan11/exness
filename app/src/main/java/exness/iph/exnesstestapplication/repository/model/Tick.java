package exness.iph.exnesstestapplication.repository.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ivanphytsyk on 5/29/17.
 */

public class Tick {
    @SerializedName("s")
    private CurrencyPair currencyPair;

    @SerializedName("b")
    private double bid;

    @SerializedName("a")
    private double ask;

    @SerializedName("spr")
    private double spread;


    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }

    public double getBid() {
        return bid;
    }

    public void setBid(double bid) {
        this.bid = bid;
    }

    public double getAsk() {
        return ask;
    }

    public void setAsk(double ask) {
        this.ask = ask;
    }

    public double getSpread() {
        return spread;
    }

    public void setSpread(double spread) {
        this.spread = spread;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tick tick = (Tick) o;

        return currencyPair == tick.currencyPair;
    }

    @Override
    public int hashCode() {
        return currencyPair.hashCode();
    }
}
