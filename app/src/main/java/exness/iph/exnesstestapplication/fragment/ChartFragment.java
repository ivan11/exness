package exness.iph.exnesstestapplication.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import exness.iph.exnesstestapplication.R;
import exness.iph.exnesstestapplication.connectivity.ClientWebSocket;
import exness.iph.exnesstestapplication.presenter.ChartPresenter;
import exness.iph.exnesstestapplication.repository.model.CurrencyPair;
import exness.iph.exnesstestapplication.repository.model.Tick;
import exness.iph.exnesstestapplication.view.ChartView;

/**
 * Created by ivanphytsyk on 5/30/17.
 */

public class ChartFragment extends Fragment implements ChartView {

    public static final String EXTRA_CURRENCY_PAIR = "extra_currency_pair";

    private LineChart lineChart;

    private List<Entry> bidEntries = new ArrayList<>();
    private List<Entry> askEntries = new ArrayList<>();

    private ChartPresenter chartPresenter;

    private boolean isChartInited;
    private LineDataSet askDataSet;
    private LineDataSet bidDataSet;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CurrencyPair currencyPair = (CurrencyPair) getArguments().getSerializable(EXTRA_CURRENCY_PAIR);
        chartPresenter = new ChartPresenter(currencyPair, this, new ClientWebSocket());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chart, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        chartPresenter.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        chartPresenter.stop();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        lineChart = (LineChart) view.findViewById(R.id.chart);
        lineChart.setScaleXEnabled(true);
        lineChart.animateXY(200, 200);
    }

    @Override
    public void addPointToChart(final Tick tick) {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Entry bidEntry = new Entry(bidEntries.size(), (float) tick.getBid());
                bidEntries.add(bidEntry);

                Entry askEntry = new Entry(askEntries.size(), (float) tick.getAsk());
                askEntries.add(askEntry);

                if (!isChartInited) {
                    isChartInited = true;
                    initChart();
                }

                bidDataSet.addEntry(bidEntry);
                askDataSet.addEntry(askEntry);
                askDataSet.notifyDataSetChanged();
                bidDataSet.notifyDataSetChanged();
                lineChart.notifyDataSetChanged();
                lineChart.setVisibleXRangeMaximum(bidEntries.size());
                lineChart.getXAxis().setAxisMaximum(bidEntries.size() + 5);
                lineChart.getXAxis().setAxisMinimum(0);

                lineChart.moveViewTo(bidEntries.size(), 2f, YAxis.AxisDependency.LEFT);
                lineChart.getAxisLeft().resetAxisMaximum();
                lineChart.getAxisLeft().resetAxisMinimum();
                lineChart.getAxisRight().resetAxisMaximum();
                lineChart.getAxisRight().resetAxisMinimum();


               // lineChart.invalidate();
            }
        });

    }

    private void initChart() {
        this.bidDataSet = new LineDataSet(bidEntries, "Bid");
        bidDataSet.setColor(getResources().getColor(R.color.colorAccent));
        bidDataSet.setDrawValues(false);

        this.askDataSet = new LineDataSet(askEntries, "Ask");
        bidDataSet.setColor(getResources().getColor(R.color.colorPrimary));
        askDataSet.setDrawValues(false);

        List<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(bidDataSet);
        dataSets.add(askDataSet);
        LineData lineData = new LineData(dataSets);
        lineChart.setData(lineData);
    }
}
