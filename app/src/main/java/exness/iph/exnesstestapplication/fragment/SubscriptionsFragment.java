package exness.iph.exnesstestapplication.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import exness.iph.exnesstestapplication.R;
import exness.iph.exnesstestapplication.presenter.SubscriptionPresenter;
import exness.iph.exnesstestapplication.repository.model.CurrencyPair;
import exness.iph.exnesstestapplication.storage.PreferenceManager;
import exness.iph.exnesstestapplication.view.SubscriptionView;

/**
 * Created by ivanphytsyk on 5/30/17.
 */

public class SubscriptionsFragment extends Fragment implements SubscriptionView {
    private List<CheckBox> checkBoxes = new ArrayList<>();

    private SubscriptionPresenter subscriptionPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        subscriptionPresenter = new SubscriptionPresenter(this, new PreferenceManager(getActivity()));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_subscriptions, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
            View child = ((ViewGroup) view).getChildAt(i);
            if (child instanceof CheckBox) {
                checkBoxes.add((CheckBox) child);
            }
        }

        view.findViewById(R.id.btn_subscribe).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subscriptionPresenter.saveClicked(getSubscriptionSet());
            }
        });

    }

    @Override
    public void moveBack() {
        getFragmentManager().popBackStack();
    }

    @Override
    public void showSubscriptions(Set<CurrencyPair> savedState) {
        for (CheckBox checkBox : checkBoxes) {
            CurrencyPair currencyPair = getCurrencyPairFromCheckboxId(checkBox.getId());
            boolean isChecked = savedState.contains(currencyPair);
            checkBox.setChecked(isChecked);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        subscriptionPresenter.start();
    }

    private CurrencyPair getCurrencyPairFromCheckboxId(int checkBoxId) {
        switch (checkBoxId) {
            case R.id.chb_aud_usd:
                return CurrencyPair.AUDUSD;
            case R.id.chb_eur_usd:
                return CurrencyPair.EURUSD;
            case R.id.chb_eur_gbr:
                return CurrencyPair.EURGBP;
            case R.id.chb_usd_jpy:
                return CurrencyPair.USDJPY;
            case R.id.chb_gbr_usd:
                return CurrencyPair.GBPUSD;
            case R.id.chb_usd_chf:
                return CurrencyPair.USDCHF;
            case R.id.chb_usd_cad:
                return CurrencyPair.USDCAD;
            case R.id.chb_eur_jpy:
                return CurrencyPair.EURJPY;
            case R.id.chb_eur_chf:
            default:
                return CurrencyPair.EURCHF;
        }
    }

    public Set<CurrencyPair> getSubscriptionSet() {
        Set<CurrencyPair> subscriptionSet = new HashSet<>();
        for (CheckBox checkBox : checkBoxes) {
            if (checkBox.isChecked()) {
                subscriptionSet.add(getCurrencyPairFromCheckboxId(checkBox.getId()));
            }
        }
        return subscriptionSet;
    }
}
