package exness.iph.exnesstestapplication.connectivity;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Set;

import exness.iph.exnesstestapplication.repository.model.Tick;

/**
 * Created by ivanphytsyk on 5/30/17.
 */

public class SubscribedList {
    @SerializedName("ticks")
    private Set<Tick> ticks;

    public Set<Tick> getTicks() {
        return ticks;
    }

    public void setTicks(Set<Tick> ticks) {
        this.ticks = ticks;
    }
}
