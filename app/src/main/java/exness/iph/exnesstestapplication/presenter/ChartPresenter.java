package exness.iph.exnesstestapplication.presenter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import exness.iph.exnesstestapplication.repository.RemoteTicksRepository;
import exness.iph.exnesstestapplication.repository.TickCallback;
import exness.iph.exnesstestapplication.repository.model.CurrencyPair;
import exness.iph.exnesstestapplication.repository.model.Tick;
import exness.iph.exnesstestapplication.view.ChartView;

/**
 * Created by ivanphytsyk on 5/30/17.
 */

public class ChartPresenter {

    private ChartView chartView;
    private RemoteTicksRepository remoteTicksRepository;
    private CurrencyPair subscription;

    public ChartPresenter(CurrencyPair subscription, ChartView chartView, RemoteTicksRepository remoteTicksRepository) {
        this.subscription = subscription;
        this.chartView = chartView;
        this.remoteTicksRepository = remoteTicksRepository;
    }

    public void start() {
        remoteTicksRepository.subscribe(new HashSet<CurrencyPair>(Arrays.asList(subscription)), new TickCallback() {
            @Override
            public void onTickReceived(List<Tick> ticks) {
                for (Tick tick: ticks) {
                    chartView.addPointToChart(tick);
                }
            }
        });
    }

    public void stop() {
        remoteTicksRepository.unsubscribe();
    }

}
